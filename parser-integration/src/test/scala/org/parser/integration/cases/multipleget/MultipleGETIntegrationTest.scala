package org.parser.integration.cases.multipleget

import org.parser.integration.cases.BaseMultipleGETIntegrationTest

/**
 * @author OlehZanevych
 */
object MultipleGETIntegrationTest extends BaseMultipleGETIntegrationTest {

  val apis = Array(
      (
          "posts",
          "/posts",
          Array(
              "title" -> "String",
              "mainPage" -> "String"
          ),
          null,
          Array(
              "title",
              "mainPage"
          )
      ),
      
      (
          "posts",
          "/posts",
          Array(
              "title" -> "String",
              "mainPage" -> "String"
          ),
          Array(
              "title" -> "String",
              "mainPage" -> "String"
          ),
          Array(
              "title",
              "mainPage"
          )
      )
  )

  val testCase = test(apis)
  
}
