package org.parser.integration

import io.gatling.core.Predef.checkBuilder2Check
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.exec
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.validatorCheckBuilder2CheckBuilder
import io.gatling.core.Predef.value2Expression

import io.gatling.core.structure.ChainBuilder

import io.gatling.http.Predef.ELFileBody
import io.gatling.http.Predef.http
import io.gatling.http.Predef.jsonPath
import io.gatling.http.Predef.status

/**
 * Implementation of methods for testing CRUD methods for all API.
 * @author OlehZanevych
 */
abstract class BaseCRUDIntegrationTest {
  
  val username = "admin"
  val password = "simple-password"
  
  def crud(title: String, uri: String, key: String, postDataFile: String, putDataFile: String,
      fields: Array[(String, String)] = null): ChainBuilder = {
    exec(post(title, uri, key, postDataFile, false, fields))
    .exec(update(title, key, putDataFile))
    .exec(delete(title, key))
  }

  def post(title: String, uri: String, key: String, postDataFile: String, saveId: Boolean = true,
      fields: Array[(String, String)] = null): ChainBuilder = {
    var scenario = http("Post " + title)
      .post(uri)
      .basicAuth(username, password)
      .header("Content-Type", "application/json")
      .body(ELFileBody("data/" + postDataFile))
      .asJSON
      .check(status.is(201))
      .check(jsonPath("$.uri").find.saveAs(key + "Uri"))
    if (saveId) {
      scenario = scenario.check(jsonPath("$.id").find.saveAs(key + "Id"))
    }
    if (fields != null) {
      fields.foreach{field => {
        scenario = scenario.check(jsonPath("$." + field._1).find.saveAs(field._2))
      }}
    }
    exec(scenario)
      .exec(checkUrl(title, key, 200))
  }
  
  def update(title: String, key: String, putDataFile: String): ChainBuilder = {
    exec(http("Update " + title)
      .put("${" + key + "Uri}")
      .basicAuth(username, password)
      .header("Content-Type", "application/json")
      .body(ELFileBody("data/" + putDataFile))
      .asJSON
      .check(status.is(200)))
    .exec(checkUrl(title, key, 200))
  }
  
  def delete(title: String, key: String): ChainBuilder = {
    exec(http("Delete " + title)
      .delete("${" + key + "Uri}")
      .basicAuth(username, password)
      .check(status.is(204)))
    .exec(checkUrl(title, key, 404))
  }
  
  def checkUrl(title: String, key: String, requiredStatus: Int): ChainBuilder = {
    exec(http("Get " + title)
      .get("${" + key + "Uri}")
      .basicAuth(username, password)
      .check(status.is(requiredStatus)))
  }

}