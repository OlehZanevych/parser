package org.parser.integration.cases.post

import org.parser.integration.BaseCRUDIntegrationTest
import io.gatling.core.Predef.exec

/**
 * Integration test of post API.
 * @author OlehZanevych
 */
object PostIntegrationTest extends BaseCRUDIntegrationTest {

  val testCase =
    exec(crud(
      "post", "/posts", "post",
      "post/data1.json", "post/data2.json"))

}