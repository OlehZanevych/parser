package org.parser.integration.cases

import io.gatling.core.Predef.scenario

import org.parser.integration.cases.multipleget.MultipleGETIntegrationTest

import org.parser.integration.cases.post.PostIntegrationTest

/**
 * All test cases, that are in system.
 * @author OlehZanevych
 */
object TestCases {

    val scn = scenario("Integration test scenario")
              .exec(
                  MultipleGETIntegrationTest.testCase,
                  
                  PostIntegrationTest.testCase
              )
  
}
