package org.parser.integration.cases

import java.util.UUID
import java.util.Calendar

import java.text.SimpleDateFormat

import scala.util.Random

import io.gatling.core.Predef.checkBuilder2Check
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.exec
import io.gatling.core.Predef.value2Expression

import io.gatling.core.structure.ChainBuilder

import io.gatling.http.Predef.http
import io.gatling.http.Predef.jsonPath
import io.gatling.http.Predef.status

/**
 * Implementation of methods for testing multiple get for all API.
 * @author OlehZanevych
 */
abstract class BaseMultipleGETIntegrationTest {
  
  val username = "admin"
  val password = "simple-password"
  
  val defaultParameters = Array(
      "offset" -> "Integer",
      "limit" -> "Integer"
  )
  
  val defaultSimpleFields = Array(
      "id" -> "Long"
  )
  
  val defaultOrders = Array(
      "id"
  )
  
  def test(apis: Array[(String, String, Array[(String, String)], Array[(String, String)], Array[String])]): ChainBuilder = {
    var result = exec()
    apis.foreach{api => {
      val title = "Multiple GET " + api._1
      val titleWith = title + " with "
      val titleFiltered = title + " filtered by "
      val titleOrdered = title + " ordered by "
      val titleAll = title + " with all parameters"
      val rootUri = api._2
      val simpleFields = defaultSimpleFields ++ api._3
      val collectionFields = api._4
      val orders = defaultOrders ++ api._5
      var fields = simpleFields.map{field => field._1}
      if (collectionFields != null) {
        fields = fields ++ collectionFields.map{field => field._1}
      }
      
      result = result.exec(checkStatusValue(title, rootUri, 200))
      
      defaultParameters.foreach{parameter => {
        val currentTitle = titleWith + parameter._1
        val currentUri = rootUri + "?" + parameter._1 + "=" + getValue(parameter._2)
        result = result.exec(checkStatusValue(currentTitle, currentUri, 200))
      }}
      
      simpleFields.foreach{simpleField => {
        val currentTitle = titleFiltered + simpleField._1
        val currentUri = rootUri + "?" + simpleField._1 + "=" + getFilter(simpleField._2)
        result = result.exec(checkStatusValue(currentTitle, currentUri, 200))
      }}
      
      if (collectionFields != null) {
        collectionFields.foreach{collectionField => {
          val currentTitle = titleFiltered + collectionField._1
          val currentUri = rootUri + "?" + collectionField._1 + "=" + getCollectionFilter(collectionField._2)
          result = result.exec(checkStatusValue(currentTitle, currentUri, 200))
        }}
      }
      
      if (orders != null) {
        orders.foreach{order => {
          val currentTitle = titleOrdered + order
          val currentUri = rootUri + "?orderBy=" + order + getOrderType
          result = result.exec(checkStatusValue(currentTitle, currentUri, 200))
        }}
      }
      
      fields.foreach{field => {
        val currentTitle = title + " " + field
        val currentUri = rootUri + "?fields=" + field
        result = result.exec(checkStatusValue(currentTitle, currentUri, 200))
      }}
      
      var fullUri = rootUri + "?" +
        defaultParameters.map{parameter => parameter._1 + "=" + getValue(parameter._2)}.mkString("&") + "&" +
        simpleFields.map{simpleField => simpleField._1 + "=" + getFilter(simpleField._2)}.mkString("&") + "&"
      if (collectionFields != null) {
        fullUri += collectionFields.map{collectionField => collectionField._1 + "=" + getCollectionFilter(collectionField._2)}
          .mkString("&") + "&"
      }
      if (orders != null) {
        fullUri += "orderBy=" + orders.map{order => order + getOrderType}.mkString(",") + "&"
      }
      fullUri += "fields=" + fields.mkString(",")
      result = result.exec(checkStatusValue(titleAll, fullUri, 200))
    }}
    result
  }
  
  def checkStatusValue(title: String, uri: String, statusValue: Int) =
    http(title)
      .get(uri)
      .basicAuth(username, password)
      .check(status.is(statusValue))
  
  def getCollectionFilter(valueType: String): String = {
    valueType match {
      case "Boolean" | "Byte" | "Short" | "Integer" | "Long" | "Float" | "Double" => getNumberFilter(valueType)
      case "String" => getStringFilter
      case default => getEnumCollectionFilter(valueType: String)
    }
  }
  
  def getFilter(valueType: String): String = {
    valueType match {
      case "Boolean" | "Byte" | "Short" | "Integer" | "Long" | "Float" | "Double" => getNumberFilter(valueType)
      case "String" => getStringFilter
      case default => getEnumFilter(valueType: String)
    }
  }
  
  def getStringFilter: String = {
    getStringValue + "," + getStringValue + ";" + getStringValue + "," + getStringValue
  }
  
  def getNumberFilter(valueType: String): String = {
    "<=" + getNumberValue(valueType) + ",<" + getNumberValue(valueType) +
      "," + getNumberValue(valueType) + ";" + ">" + getNumberValue(valueType) +
      ",>=" + getNumberValue(valueType) + "," + getNumberValue(valueType)
  }
  
  def getValue(valueType: String): String = {
    valueType match {
      case "Boolean" | "Byte" | "Short" | "Integer" | "Long" | "Float" | "Double" => getNumberValue(valueType)
      case "String" => getStringFilter
      case default => getEnumValue(valueType: String)
    }
  }
  
  def getEnumCollectionFilter(valueType: String): String = {
    getEnumFilter(valueType) + ";" + getEnumFilter(valueType)
  }
  
  def getEnumFilter(valueType: String): String = {
    getEnumValue(valueType) + "," + getEnumValue(valueType)
  }
  
  def getNumberValue(valueType: String): String = {
    val random = new Random
    valueType match {
      case "Boolean" => random.nextBoolean.toString
      case "Byte" => (random.nextInt(3) + 1).toString
      case "Short" => (random.nextInt(3000) + 33).toString
      case "Integer" => (random.nextInt(300000) + 333).toString
      case "Long" => (random.nextInt(3000000) + 3333).toString
      
      case "Float" => (random.nextFloat + 3F).toString
      case "Double" => (random.nextDouble + 3D).toString
    }
  }
  
  def getStringValue: String = {
    val random = new Random
    random.alphanumeric.take(random.nextInt(7) + 3).mkString
  }
  
  def getEnumValue(valueType: String): String = {
    val random = new Random
    valueType match {
      case "AcademicStatus" => random.nextInt(3) match {
        case 0 => "SENIOR_RESEARCHER"
        case 1 => "ASSOCIATE_PROFESSOR"
        case 2 => "PROFESSOR"
      }
      
      case "ClassType" => random.nextInt(3) match {
        case 0 => "LECTURE"
        case 1 => "LABORATORY"
        case 2 => "PRACTICAL"
      }
      
      case "CourseType" => random.nextInt(3) match {
        case 0 => "NORMATIVE"
        case 1 => "STUDENT_CHOICE"
        case 2 => "UNIVERSITY_CHOICE"
      }
      
      case "Day" => random.nextInt(7) match {
        case 0 => "SUNDAY"
        case 1 => "MONDAY"
        case 2 => "TUESDAY"
        case 3 => "WEDNESDAY"
        case 4 => "THURSDAY"
        case 5 => "FRIDAY"
        case 6 => "SATURDAY"
      }
      
      case "EducationalDegree" => random.nextInt(3) match {
        case 0 => "BACHELOR"
        case 1 => "SPECIALIST"
        case 2 => "MASTER"
      }
      
      case "Report" => random.nextInt(6) match {
        case 0 => "EXAM"
        case 1 => "SETOFF"
        case 2 => "DIFFERENTIATED_SETOFF"
        case 3 => "COURSE_PROJECT"
        case 4 => "COURSEWORK"
        case 5 => "ELECTIVE"
      }
      
      case "WeekPeriodicity" => random.nextInt(7) match {
        case 0 => "ALWAYS"
        case 1 => "NUMERATOR"
        case 2 => "DENOMINATOR"
        case 3 => "NUMERATOR_OF_NUMERATOR"
        case 4 => "DENOMINATOR_OF_NUMERATOR"
        case 5 => "NUMERATOR_OF_DENOMINATOR"
        case 6 => "DENOMINATOR_OF_DENOMINATOR"
      }
    }
  }
  
  def getOrderType: String = {
    val random = new Random
    random.nextInt(2) match {
      case 0 => "-asc"
      case 1 => "-desc"
    }
  }
        
}
