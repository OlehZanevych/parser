package org.parser.web.rest.constant;

/**
 * Class for controller requests.
 * @author OlehZanevych
 */
public class Request {

	public static final String ID = "/{id}";
	public static final String KEY = "/{key}";
	
}
