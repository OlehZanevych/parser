package org.parser.web.rest.controller.post;

import javax.annotation.Resource;

import org.parser.facade.facade.Facade;
import org.parser.resource.post.PostResource;
import org.parser.resource.message.MessageResource;
import org.parser.resource.message.MessageType;
import org.parser.resource.search.PagedRequest;
import org.parser.resource.search.PagedResultResource;
import org.parser.web.rest.constant.Request;
import org.parser.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Controller, that handles all API with Post entity.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/posts")
@Api(value = "post", description = "Posts")
public class PostController extends BaseController {
	
	private static final Logger LOG = LoggerFactory.getLogger(PostController.class);
	
	@Resource(name = "postFacade")
	private Facade<PostResource, Long> facade;
	
	/**
	 * Method for creating new PostResource.
	 * @param postResource PostResource.
	 * @return PostResource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create Post")
	public PostResource createResource(@RequestBody final PostResource postResource) {
		LOG.info("Creating post: {}", postResource);
		return facade.createResource(postResource);
	}
	
	/**
	 * Method for updating PostResource.
	 * @param id PostResource identifier.
	 * @param postResource updated PostResource.
	 * @return notification of update PostResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = Request.ID, method = RequestMethod.PUT)
	@ApiOperation(value = "Update Post")
	public MessageResource updateResource(@PathVariable("id") final Long id, @RequestBody final PostResource postResource) {
		LOG.info("Updated post with id: {}, {}", id, postResource);
		facade.updateResource(id, postResource);
		return new MessageResource(MessageType.INFO, "Post Updated");
	}
	
	/**
	 * Method for getting PostResource.
	 * @param id PostResource identifier.
	 * @return PostResource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = Request.ID, method = RequestMethod.GET)
	@ApiOperation(value = "Get Post by id")
	public PostResource getResource(@PathVariable("id") final Long id) {
		LOG.info("Retrieving post with id: {}", id);
		return facade.getResource(id);
	}
	
	/**
	 * Method for removing PostResource.
	 * @param id PostResource identifier id.
	 * @return notification of deletion PostResource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = Request.ID, method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Post by id")
	public MessageResource removeResource(@PathVariable("id") final Long id) {
		LOG.info("Removing post with id: {}", id);
		facade.removeResource(id);
		return new MessageResource(MessageType.INFO, "Post removed");
	}
	
	/**
	 * Method that returns a page of PostResources.
	 * @param request request.
	 * @return page of PostResources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get Posts")
	public PagedResultResource<PostResource> getPagedResource(final PagedRequest request) {
		LOG.info("Retrieving PagedResultResource for Post Resources with offset: {}, limit: {}", request.getOffset(), request.getLimit());
		return facade.getResources(request);
	}
	
}
