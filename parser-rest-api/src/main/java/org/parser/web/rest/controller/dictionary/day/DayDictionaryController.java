package org.parser.web.rest.controller.dictionary.day;

import java.util.LinkedHashMap;

import javax.annotation.Resource;

import org.parser.domain.dictionary.Value;
import org.parser.domain.enumtype.day.Day;
import org.parser.facade.dictionary.DictionaryFacade;
import org.parser.web.rest.constant.Request;
import org.parser.web.rest.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Controller, that handles getting APIs for Day dictionary.
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/dictionaries/days")
@Api(value = "days", description = "Getting dictionary APIs")
public class DayDictionaryController extends BaseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DayDictionaryController.class);
	
	@Resource(name = "dayDictionaryFacade")
	private DictionaryFacade<Day> dictionaryFacade;
	
	/**
	 * Method for getting value from Day dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return value for key
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = Request.KEY, method = RequestMethod.GET)
	@ApiOperation(value = "Get day description by key")
	public Value getValue(@PathVariable("key") final Day key) {
		LOGGER.info("Retrieving value from Day dictionary by key '{0}'", key);
		return dictionaryFacade.getValue(key);
	}
	
	/**
	 * Method for getting all Day dictionary.
	 * 
	 * @return Day dictionary
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get all dictionary for days")
	public LinkedHashMap<Day, Value> getDictionary() {
		LOGGER.info("Retrieving all Day dictionary");
		return dictionaryFacade.getDictionary();
	}
	
}
