#!/bin/bash

BACKEND_PATH=`pwd`
MVN_PATH=$(cd tools/maven; pwd)

COMMAND=$1
ARGS=${@:2}

function main() {
      case "$COMMAND" in
      build) build $ARGS
          ;;
      run) run $ARGS
          ;;
      test) integrationTest $ARGS
          ;;
      migrate) migrate $ARGS
          ;;
      dropDb) dropDb $ARGS
	  ;;
      clean) clean $ARGS
          ;;
      seed) seed $ARGS
          ;;
      *) echo "invalid command: build.sh build|run|test|migrate|seed"
          ;;
      esac
}

# build project
function build() {
    ${MVN_PATH}/bin/mvn clean install ${@:1}
}

# set up database
function migrate() {
    cd ${BACKEND_PATH}/parser-db-structure
    ${MVN_PATH}/bin/mvn liquibase:update ${@:1}
}

function dropDb() {
    cd ${BACKEND_PATH}/parser-db-structure
    ${MVN_PATH}/bin/mvn liquibase:dropAll ${@:1}
}

# clean db data
function clean() {
    cd ${BACKEND_PATH}/parser-sql
    ${MVN_PATH}/bin/mvn -Pcleaning clean install ${@:1}
}

# seed db with data
function seed() {
    cd ${BACKEND_PATH}/parser-sql
    ${MVN_PATH}/bin/mvn -Pdata clean install ${@:1}
}

# start tomcat using maven plugin
function run() {
    ${MVN_PATH}/bin/mvn clean install ${@:1}
    cd ${BACKEND_PATH}/parser-rest-api
    ${MVN_PATH}/bin/mvn tomcat7:run ${@:1}
}

# run integration tests
function integrationTest() {
    cd ${BACKEND_PATH}/parser-integration
    ${MVN_PATH}/bin/mvn clean install ${@:1}
}

main
