package org.parser.facade.facade;

import org.parser.converter.Converter;
import org.parser.domain.Model;
import org.parser.domain.enumtype.api.API;
import org.parser.domain.enumtype.apimethodtype.APIMethodType;
import org.parser.pagination.PagedResult;
import org.parser.resource.APIResource;
import org.parser.resource.search.PagedRequest;
import org.parser.resource.search.PagedResultResource;
import org.parser.service.Service;
import org.parser.service.session.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Facade implementation.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity.
 * @param <RESOURCE> Resource.
 * @param <SERVICE> Service.
 * @param <KEY> Key.
 */
@Transactional
public class DefaultFacade<ENTITY extends Model, RESOURCE extends APIResource<KEY>,
	SERVICE extends Service<ENTITY, KEY>, KEY extends Number> implements Facade<RESOURCE, KEY> {
	
	private API api;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFacade.class);

	private Converter<RESOURCE, ENTITY> resourceConverter;

	private Converter<ENTITY, RESOURCE> entityConverter;

	private SERVICE service;
	
	@javax.annotation.Resource(name = "defaultSessionService")
	private SessionService sessionService;

	@Override
	public RESOURCE createResource(final RESOURCE resource) {
		
		LOGGER.info("Creating resource: {}", resource);
		
		sessionService.verifyAPIMethodAccess(api, APIMethodType.CREATE);

		ENTITY entity = resourceConverter.convert(resource);

		service.createEntity(entity);

		return entityConverter.convert(entity);
	}

	@Override
	public RESOURCE getResource(final KEY id) {
		
		LOGGER.info("Getting resource with id: {}", id);
		
		sessionService.verifyAPIMethodAccess(api, APIMethodType.GET);
		
		ENTITY entity = service.getEntity(id);
		
		RESOURCE resource = entityConverter.convert(entity);
		
		return resource;
	}

	@Override
	public void removeResource(final KEY id) {
		
		LOGGER.info("Removing resource with id: {}", id);
		
		sessionService.verifyAPIMethodAccess(api, APIMethodType.MODIFY);
		
		ENTITY entity = service.getEntity(id);
		
		service.removeEntity(entity);
	}

	@Override
	public void updateResource(final KEY id, final RESOURCE resource) {
		
		LOGGER.info("Updating resource with id: {}, resource: {}", id, resource);
		
		sessionService.verifyAPIMethodAccess(api, APIMethodType.MODIFY);

		ENTITY entity = service.getEntity(id);
		
		resourceConverter.convert(resource, entity);
		
		service.updateEntity(entity);

	}

	@Override
	public PagedResultResource<RESOURCE> getResources(final PagedRequest request) {
		
		LOGGER.info("Getting paged result resource");
		
		sessionService.verifyAPIMethodAccess(api, APIMethodType.GET);

		PagedResult<ENTITY> pagedResult = service.getEntities(request);

		List<RESOURCE> resources = entityConverter.convertAll(pagedResult.getEntities());

		return new PagedResultResource<>(pagedResult.getOffset(), pagedResult.getLimit(), pagedResult.getCount(), resources);
	}
	
	@Required
	public void setApi(final API api) {
		this.api = api;
	}

	@Required
	public void setService(final SERVICE service) {
		this.service = service;
	}
	
	@Required
	public void setResourceConverter(final Converter<RESOURCE, ENTITY> resourceConverter) {
		this.resourceConverter = resourceConverter;
	}
	
	@Required
	public void setEntityConverter(final Converter<ENTITY, RESOURCE> entityConverter) {
		this.entityConverter = entityConverter;
	}
	
	public API getApi() {
		return api;
	}

	public SERVICE getService() {
		return service;
	}

	public Converter<RESOURCE, ENTITY> getResourceConverter() {
		return resourceConverter;
	}

	public Converter<ENTITY, RESOURCE> getEntityConverter() {
		return entityConverter;
	}
	
}
