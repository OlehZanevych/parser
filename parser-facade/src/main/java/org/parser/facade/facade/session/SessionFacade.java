package org.parser.facade.facade.session;

import org.parser.resource.session.SessionResource;

/**
 * Interface for declaring session functionality.
 * @author OlehZanevych
 *
 */
public interface SessionFacade {

	/**
	 * Method for getting current session.
	 * @return current session.
	 */
	SessionResource getCurrentSession();

}
