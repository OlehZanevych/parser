package org.parser.facade.facade;

import org.parser.resource.search.PagedRequest;
import org.parser.resource.search.PagedResultResource;

/**
 * Common interface for facades.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource.
 * @param <KEY> Key.
 */
public interface Facade<RESOURCE, KEY extends Number> {
	
	/**
	 * Method for creating entity.
	 * @param resource
	 * @return Entity resource.
	 */
	RESOURCE createResource(RESOURCE resource);

	/**
	 * Method for getting entity.
	 * 
	 * @param id
	 * @return Entity resource.
	 */
	RESOURCE getResource(final KEY id);

	/**
	 * Method for removing entity.
	 * 
	 * @param id
	 */
	void removeResource(final KEY id);

	/**
	 * Method for updating entity.
	 * 
	 * @param id
	 * @param resource
	 */
	void updateResource(final KEY id, RESOURCE resource);

	/**
	 * Method for getting paged result for entities.
	 * 
	 * @param request paged request
	 * @return Paged Result.
	 */
	PagedResultResource<RESOURCE> getResources(PagedRequest request);
}
