package org.parser.facade.dictionary;

import java.util.LinkedHashMap;

import org.parser.domain.dictionary.Value;

/**
 * Interface for dictionary facades.
 * @author OlehZanevych
 *
 * @param <KEY> Enum type of dictionary key
 */
public interface DictionaryFacade<KEY extends Enum<?>> {

	/**
	 * Method for getting value from dictionary by key.
	 * 
	 * @param key key in dictionary
	 * @return value for key
	 */
	Value getValue(KEY key);
	
	/**
	 * Method for getting all dictionary.
	 * 
	 * @return dictionary
	 */
	LinkedHashMap<KEY, Value> getDictionary();

}
