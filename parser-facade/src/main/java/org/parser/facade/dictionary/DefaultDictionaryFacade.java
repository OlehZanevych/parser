package org.parser.facade.dictionary;

import java.util.LinkedHashMap;

import org.parser.domain.dictionary.Value;
import org.parser.service.dictionary.DictionaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Default dictionary facade implementation.
 * @author OlehZanevych
 *
 * @param <KEY> Enum type of dictionary key
 */
public class DefaultDictionaryFacade<KEY extends Enum<?>> implements DictionaryFacade<KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDictionaryFacade.class);

	private DictionaryService<KEY> dictionaryService;
	
	@Override
	public Value getValue(final KEY key) {
		LOGGER.info("Getting value from dictionary by key '{0}'", key);
		return dictionaryService.getValue(key);
	}

	@Override
	public LinkedHashMap<KEY, Value> getDictionary() {
		LOGGER.info("Getting all dictionary");
		return dictionaryService.getDictionary();
	}
	
	@Required
	public void setDictionaryService(final DictionaryService<KEY> dictionaryService) {
		this.dictionaryService = dictionaryService;
	}

	public DictionaryService<KEY> getDictionaryService() {
		return dictionaryService;
	}

}
