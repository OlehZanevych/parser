package org.parser.annotation.dbtable;

/**
 * Annotation, that is used to describe system table.
 * @author AnnaVolynets
 */
public @interface System {

}
