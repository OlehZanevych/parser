package org.parser.annotation.dbtable;

/**
 * Annotation, that is used to describe table which contains essence.
 * @author AnnaVolynets
 */
public @interface Essence {

}
