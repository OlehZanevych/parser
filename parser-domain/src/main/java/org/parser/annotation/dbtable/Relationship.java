package org.parser.annotation.dbtable;

/**
 * Annotation, that is used to describe table which is forming a relationship many to many.
 * @author AnnaVolynets
 */
public @interface Relationship {

}
