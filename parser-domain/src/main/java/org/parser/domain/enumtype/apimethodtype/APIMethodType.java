package org.parser.domain.enumtype.apimethodtype;

/**
 * Enum for storing API method types.
 * @author AnnaVolynets
 */
public enum APIMethodType {
	GET,
	CREATE,
	MODIFY
}
