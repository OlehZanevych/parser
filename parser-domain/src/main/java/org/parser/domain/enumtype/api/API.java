package org.parser.domain.enumtype.api;

/**
 * Enum for storing API names.
 * @author AnnaVolynets
 */
public enum API {
	posts
}
