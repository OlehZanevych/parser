package org.parser.domain.enumtype.day;

/**
 * Enum, that describes day.
 * @author AnnaVolynets
 */
public enum Day {
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
}
