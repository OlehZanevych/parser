package org.parser.domain.post;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.parser.annotation.dbtable.Essence;
import org.parser.domain.ModelWithSimpleId;

/**
 * Entity, that describes post.
 * @author AnnaVolynets
 */
@Essence
@Entity
@Table(name = "t_essence_posts")
public class Post extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@Column(name = "title")
	private String title;
	
	@NotNull
	@Column(name = "main_page")
	private String mainPage;

	/**
	 * Default constructor with no parameters.
	 */
	public Post() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Post(final Long id) {
		super(id);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getMainPage() {
		return mainPage;
	}

	public void setMainPage(final String mainPage) {
		this.mainPage = mainPage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((mainPage == null) ? 0 : mainPage.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Post other = (Post) obj;
		if (mainPage == null) {
			if (other.mainPage != null) {
				return false;
			}
		} else if (!mainPage.equals(other.mainPage)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Post [title=");
		builder.append(title);
		builder.append(", mainPage=");
		builder.append(mainPage);
		builder.append("]");
		return builder.toString();
	}
	
}
