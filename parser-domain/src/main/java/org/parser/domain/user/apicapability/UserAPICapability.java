package org.parser.domain.user.apicapability;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.parser.annotation.dbtable.System;
import org.parser.domain.Model;
import org.parser.domain.enumtype.api.API;
import org.parser.domain.enumtype.apimethodtype.APIMethodType;
import org.parser.domain.id.userapi.UserAPI;
import org.parser.domain.user.User;

/**
 * Entity, that describes API capability of user.
 * @author AnnaVolynets
 */
@System
@IdClass(UserAPI.class)
@Entity
@Table(name = "v_system_user_api_capabilities")
public class UserAPICapability extends Model {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Id
	@Type(type = "org.parser.domain.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.parser.domain.enumtype.api.API"
			)
	)
	@Column(name = "api", columnDefinition = "e_api")
	@Enumerated(EnumType.STRING)
	private API api;
	
	@Type(type = "org.parser.domain.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.parser.domain.enumtype.apimethodtype.APIMethodType"
			)
	)
	@Column(name = "api_method_type", columnDefinition = "e_api_method_type")
	@Enumerated(EnumType.STRING)
	private APIMethodType apiMethodType;

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	public APIMethodType getApiMethodType() {
		return apiMethodType;
	}

	public void setApiMethodType(final APIMethodType apiMethodType) {
		this.apiMethodType = apiMethodType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((apiMethodType == null) ? 0 : apiMethodType.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserAPICapability other = (UserAPICapability) obj;
		if (api != other.api) {
			return false;
		}
		if (apiMethodType != other.apiMethodType) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserAPICapability [api=" + api + ", apiMethodType=" + apiMethodType + "]";
	}

}
