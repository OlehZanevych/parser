package org.parser.domain.user;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.parser.annotation.dbtable.System;
import org.parser.domain.ModelWithSimpleId;
import org.parser.domain.enumtype.api.API;
import org.parser.domain.role.Role;
import org.parser.domain.user.apicapability.UserAPICapability;

/**
 * User entity.
 * @author AnnaVolynets
 */
@System
@Entity
@Table(name = "t_system_users")
public class User extends ModelWithSimpleId<Long> {
    
	private static final long serialVersionUID = 1L;

    @Column(name = "login")
    private String login;
    
    @Column(name = "password_hash")
    private String passwordHash;
    
    @Column(name = "surname")
    private String surname;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "middle_name")
    private String middleName;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone")
    private String phone;
    
    @ManyToMany
    @JoinTable(name = "t_relationship_users_roles",
    	joinColumns = @JoinColumn(name = "user_id"),
    	inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles;
    
    @OneToMany(mappedBy = "user")
    @MapKey(name = "api")
    private Map<API, UserAPICapability> userAPICapabilities;
    
    /**
	 * Default constructor with no parameters.
	 */
	public User() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public User(final Long id) {
		super(id);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(final String login) {
		this.login = login;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(final String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(final List<Role> roles) {
		this.roles = roles;
	}

	public Map<API, UserAPICapability> getUserAPICapabilities() {
		return userAPICapabilities;
	}

	public void setUserAPICapabilities(final Map<API, UserAPICapability> userAPICapabilities) {
		this.userAPICapabilities = userAPICapabilities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((passwordHash == null) ? 0 : passwordHash.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((userAPICapabilities == null) ? 0 : userAPICapabilities.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (middleName == null) {
			if (other.middleName != null) {
				return false;
			}
		} else if (!middleName.equals(other.middleName)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (passwordHash == null) {
			if (other.passwordHash != null) {
				return false;
			}
		} else if (!passwordHash.equals(other.passwordHash)) {
			return false;
		}
		if (phone == null) {
			if (other.phone != null) {
				return false;
			}
		} else if (!phone.equals(other.phone)) {
			return false;
		}
		if (roles == null) {
			if (other.roles != null) {
				return false;
			}
		} else if (!roles.equals(other.roles)) {
			return false;
		}
		if (surname == null) {
			if (other.surname != null) {
				return false;
			}
		} else if (!surname.equals(other.surname)) {
			return false;
		}
		if (userAPICapabilities == null) {
			if (other.userAPICapabilities != null) {
				return false;
			}
		} else if (!userAPICapabilities.equals(other.userAPICapabilities)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "User [login=" + login + ", passwordHash=" + passwordHash + ", surname=" + surname + ", name=" + name
				+ ", middleName=" + middleName + ", email=" + email + ", phone=" + phone + ", roles=" + roles
				+ ", userAPICapabilities=" + userAPICapabilities + ", id=" + id + "]";
	}

}
