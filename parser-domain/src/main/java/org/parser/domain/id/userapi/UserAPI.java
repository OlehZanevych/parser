package org.parser.domain.id.userapi;

import java.io.Serializable;

import org.parser.domain.enumtype.api.API;
import org.parser.domain.user.User;

/**
 * A composite primary key with User and API.
 * @author AnnaVolynets
 */
public class UserAPI implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private User user;
	
	private API api;
	
	/**
	 * Default constructor with no parameters.
	 */
	public UserAPI() {
		
	}

	/**
	 * Constructor with all key fields.
	 * @param user User.
	 * @param api API.
	 */
	public UserAPI(final User user, final API api) {
		this.user = user;
		this.api = api;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserAPI other = (UserAPI) obj;
		if (api != other.api) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserAPI [user=" + user + ", api=" + api + "]";
	}
	
}
