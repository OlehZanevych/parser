package org.parser.domain.id.roleapi;

import java.io.Serializable;

import org.parser.domain.enumtype.api.API;
import org.parser.domain.role.Role;

/**
 * A composite primary key with Role and API.
 * @author AnnaVolynets
 */
public class RoleAPI implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Role role;
	
	private API api;
	
	/**
	 * Default constructor with no parameters.
	 */
	public RoleAPI() {
		
	}

	/**
	 * Constructor with all key fields.
	 * @param role Role.
	 * @param api API.
	 */
	public RoleAPI(final Role role, final API api) {
		this.role = role;
		this.api = api;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(final Role role) {
		this.role = role;
	}

	public API getAPI() {
		return api;
	}

	public void setAPI(final API api) {
		this.api = api;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RoleAPI other = (RoleAPI) obj;
		if (api != other.api) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "RoleAPI [role=" + role + ", api=" + api + "]";
	}
	
}
