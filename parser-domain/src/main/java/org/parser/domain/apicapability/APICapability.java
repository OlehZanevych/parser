package org.parser.domain.apicapability;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.parser.annotation.dbtable.System;
import org.parser.domain.Model;
import org.parser.domain.enumtype.api.API;
import org.parser.domain.enumtype.apimethodtype.APIMethodType;
import org.parser.domain.id.roleapi.RoleAPI;
import org.parser.domain.role.Role;

/**
 * Entity, that describes API capability of Role.
 * @author AnnaVolynets
 */
@System
@IdClass(RoleAPI.class)
@Entity
@Table(name = "t_system_api_capabilities")
public class APICapability extends Model {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	@Id
	@Type(type = "org.parser.domain.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.parser.domain.enumtype.api.API"
			)
	)
	@Column(name = "api", columnDefinition = "e_api")
	@Enumerated(EnumType.STRING)
	private API api;
	
	@Type(type = "org.parser.domain.enumtype.postgresql.PostgreSQLEnumType",
			parameters = @Parameter(
					name = "enumClassName",
					value = "org.parser.domain.enumtype.apimethodtype.APIMethodType"
			)
	)
	@Column(name = "api_method_type", columnDefinition = "e_api_method_type")
	@Enumerated(EnumType.STRING)
	private APIMethodType apiMethodType;

	public Role getRole() {
		return role;
	}

	public void setRole(final Role role) {
		this.role = role;
	}

	public API getAPI() {
		return api;
	}

	public void setAPI(final API api) {
		this.api = api;
	}

	public APIMethodType getAPIMethodType() {
		return apiMethodType;
	}

	public void setAPIMethodType(final APIMethodType apiMethodType) {
		this.apiMethodType = apiMethodType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((apiMethodType == null) ? 0 : apiMethodType.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		APICapability other = (APICapability) obj;
		if (api != other.api) {
			return false;
		}
		if (apiMethodType != other.apiMethodType) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "APICapability [role=" + role + ", api=" + api + ", apiMethodType=" + apiMethodType + "]";
	}

}
