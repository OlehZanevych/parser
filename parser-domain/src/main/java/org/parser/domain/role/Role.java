package org.parser.domain.role;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.parser.annotation.dbtable.System;
import org.parser.domain.ModelWithSimpleId;
import org.parser.domain.apicapability.APICapability;

/**
 * Role table.
 * @author AnnaVolynets
 */
@System
@Entity
@Table(name = "t_system_roles")
public class Role extends ModelWithSimpleId<Long> {
	
	private static final long serialVersionUID = 1L;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;
    
    @OneToMany(mappedBy = "role")
	private List<APICapability> apiCapabilities;
    
    /**
	 * Default constructor with no parameters.
	 */
	public Role() {
		
	}

	/**
	 * Constructor with id.
	 * @param id identifier
	 */
	public Role(final Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public List<APICapability> getAPICapabilities() {
		return apiCapabilities;
	}

	public void setAPICapabilities(final List<APICapability> apiCapabilities) {
		this.apiCapabilities = apiCapabilities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Role other = (Role) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Role [name=" + name + ", description=" + description + ", id=" + id + "]";
	}
	
}
