package org.parser.resource.message;

/**
 * Type of message, that we can send to response.
 * @author AnnaVolynets
 *
 */
public enum MessageType {
	ERROR, INFO;
}
