package org.parser.resource;

import java.text.MessageFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Abstract class, that all API resources need to implement.
 * @author AnnaVolynets
 * 
 * @param <KEY> Identifier class, that extends Number.
 */
public abstract class APIResource<KEY extends Number> extends Resource<KEY> {
	
	/**
	 * Method, that is used for getting uri for list (multiple resources).
	 * @return root uri.
	 */
	@JsonIgnore
	public abstract String getRootUri();
	
	/**
	 * Method for getting unique uri for all resources.
	 * @return uri of string representation.
	 */
	public String getUri() {
		if (id == null) {
			return null;
		}
		return MessageFormat.format("{0}/{1}", getRootUri(), id);
	}

	@Override
	public String toString() {
		return "APIResource [id=" + id + "]";
	}
	
}
