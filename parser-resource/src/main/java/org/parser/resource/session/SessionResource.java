package org.parser.resource.session;

import org.parser.resource.user.UserResource;

/**
 * Session Resource class.
 * @author AnnaVolynets
 *
 */
public class SessionResource {

	private UserResource userResource;

	public UserResource getUserResource() {
		return userResource;
	}

	public void setUserResource(final UserResource userResource) {
		this.userResource = userResource;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userResource == null) ? 0 : userResource.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SessionResource other = (SessionResource) obj;
		if (userResource == null) {
			if (other.userResource != null) {
				return false;
			}
		} else if (!userResource.equals(other.userResource)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SessionResource [userResource=" + userResource + "]";
	}

}
