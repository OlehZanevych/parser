package org.parser.resource.search;

import org.parser.annotation.Limit;
import org.parser.annotation.Offset;

import java.util.Arrays;
import java.util.Map;


/**
/**
 * Paged Request, that comes from controller.
 * @author AnnaVolynets
 */
public class PagedRequest {

	@Offset
    private Integer offset;
    
	@Limit
	private Integer limit;
	
	private Map<String, String> parameters;
	
	private String[] orders;
	
	private String[] fields;

	/**
	 * Default constructor.
	 */
    public PagedRequest() {
    	
	}
	
	/**
	 * Constructor with all parameters.
	 * @param offset offset
	 * @param limit limit
	 * @param parameters parameters
	 * @param orders orders
	 * @param fields fields
	 */
	public PagedRequest(final Integer offset, final Integer limit, final Map<String, String> parameters,
			final String[] orders, final String[] fields) {
		
		this.offset = offset;
		this.limit = limit;
		this.parameters = parameters;
		this.orders = orders;
		this.fields = fields;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(final Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(final Integer limit) {
		this.limit = limit;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(final Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public String[] getOrders() {
		return orders;
	}

	public void setOrders(final String[] orders) {
		this.orders = orders;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(final String[] fields) {
		this.fields = fields;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(fields);
		result = prime * result + ((limit == null) ? 0 : limit.hashCode());
		result = prime * result + ((offset == null) ? 0 : offset.hashCode());
		result = prime * result + Arrays.hashCode(orders);
		result = prime * result + ((parameters == null) ? 0 : parameters.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PagedRequest other = (PagedRequest) obj;
		if (!Arrays.equals(fields, other.fields)) {
			return false;
		}
		if (limit == null) {
			if (other.limit != null) {
				return false;
			}
		} else if (!limit.equals(other.limit)) {
			return false;
		}
		if (offset == null) {
			if (other.offset != null) {
				return false;
			}
		} else if (!offset.equals(other.offset)) {
			return false;
		}
		if (!Arrays.equals(orders, other.orders)) {
			return false;
		}
		if (parameters == null) {
			if (other.parameters != null) {
				return false;
			}
		} else if (!parameters.equals(other.parameters)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PagedRequest [offset=");
		builder.append(offset);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", parameters=");
		builder.append(parameters);
		builder.append(", orders=");
		builder.append(Arrays.toString(orders));
		builder.append(", fields=");
		builder.append(Arrays.toString(fields));
		builder.append("]");
		return builder.toString();
	}
	
}
