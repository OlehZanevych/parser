package org.parser.resource.post;

import java.text.MessageFormat;

import org.parser.annotation.CrudableResource;
import org.parser.domain.enumtype.api.API;
import org.parser.resource.APIResource;
/**
 * Resource for web layer, that describes Post.
 * @author AnnaVolynets
 */
@CrudableResource
public class PostResource extends APIResource<Long> {
	
	private String title;
	
	private String mainPage;
	
	@Override
	public String getRootUri() {
		return MessageFormat.format("/{0}", API.posts);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getMainPage() {
		return mainPage;
	}

	public void setMainPage(final String mainPage) {
		this.mainPage = mainPage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((mainPage == null) ? 0 : mainPage.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PostResource other = (PostResource) obj;
		if (mainPage == null) {
			if (other.mainPage != null) {
				return false;
			}
		} else if (!mainPage.equals(other.mainPage)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PostResource [title=");
		builder.append(title);
		builder.append(", mainPage=");
		builder.append(mainPage);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
