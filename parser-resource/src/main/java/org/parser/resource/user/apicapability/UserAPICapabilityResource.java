package org.parser.resource.user.apicapability;

import org.parser.annotation.CrudableResource;
import org.parser.domain.enumtype.api.API;
import org.parser.domain.enumtype.apimethodtype.APIMethodType;

/**
 * Resource for web layer, that describes UserAPICapability.
 * @author AnnaVolynets
 */
@CrudableResource
public class UserAPICapabilityResource {
	
	private API api;
	
	private APIMethodType apiMethodType;

	public API getApi() {
		return api;
	}

	public void setApi(final API api) {
		this.api = api;
	}

	public APIMethodType getApiMethodType() {
		return apiMethodType;
	}

	public void setApiMethodType(final APIMethodType apiMethodType) {
		this.apiMethodType = apiMethodType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((api == null) ? 0 : api.hashCode());
		result = prime * result + ((apiMethodType == null) ? 0 : apiMethodType.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserAPICapabilityResource other = (UserAPICapabilityResource) obj;
		if (api != other.api) {
			return false;
		}
		if (apiMethodType != other.apiMethodType) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserAPICapabilityResource [api=" + api + ", apiMethodType=" + apiMethodType + "]";
	}

}
