package org.parser.resource.user;

import java.util.List;

import org.parser.annotation.CrudableResource;
import org.parser.resource.user.apicapability.UserAPICapabilityResource;

/**
 * Resource for web layer, that describes User.
 * @author AnnaVolynets
 */
@CrudableResource
public class UserResource {
	
	private String login;
	
	private String surname;
	
	private String name;
	
	private String middleName;
	
	private String email;
	
	private String phone;
	
	private List<UserAPICapabilityResource> userAPICapabilities;

	public String getLogin() {
		return login;
	}

	public void setLogin(final String login) {
		this.login = login;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(final String middleName) {
		this.middleName = middleName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(final String phone) {
		this.phone = phone;
	}

	public List<UserAPICapabilityResource> getUserAPICapabilities() {
		return userAPICapabilities;
	}

	public void setUserAPICapabilities(final List<UserAPICapabilityResource> userAPICapabilities) {
		this.userAPICapabilities = userAPICapabilities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((userAPICapabilities == null) ? 0 : userAPICapabilities.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserResource other = (UserResource) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (middleName == null) {
			if (other.middleName != null) {
				return false;
			}
		} else if (!middleName.equals(other.middleName)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (phone == null) {
			if (other.phone != null) {
				return false;
			}
		} else if (!phone.equals(other.phone)) {
			return false;
		}
		if (surname == null) {
			if (other.surname != null) {
				return false;
			}
		} else if (!surname.equals(other.surname)) {
			return false;
		}
		if (userAPICapabilities == null) {
			if (other.userAPICapabilities != null) {
				return false;
			}
		} else if (!userAPICapabilities.equals(other.userAPICapabilities)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserResource [login=" + login + ", surname=" + surname + ", name=" + name + ", middleName=" + middleName
				+ ", email=" + email + ", phone=" + phone + ", userAPICapabilities=" + userAPICapabilities + "]";
	}

}
