package org.parser.service.user;

import org.parser.domain.user.User;

/**
 * User Service interface.
 * @author OlehZanevych
 *
 */
public interface UserService {

	/**
	 * Method for getting user by login.
	 * @param login
	 * @return User
	 */
	User getUserByLogin(String login);
	
}
