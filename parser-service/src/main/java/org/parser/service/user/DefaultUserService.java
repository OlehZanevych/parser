package org.parser.service.user;

import org.parser.dao.dao.user.UserDao;
import org.parser.domain.user.User;
import org.parser.service.DefaultService;

/**
 * Default User Service.
 * @author OlehZanevych
 */
public class DefaultUserService extends DefaultService<User, Long, UserDao> implements UserService {

	@Override
	public User getUserByLogin(final String login) {
		return getDao().getUserByLogin(login);
	}

}
