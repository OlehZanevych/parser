package org.parser.service;

import org.parser.pagination.PagedResult;
import org.parser.resource.search.PagedRequest;

/**
 * Common interface for all services.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity.
 * @param <KEY> Key.
 */
public interface Service<ENTITY, KEY extends Number> {

	/**
	 * Method for creating entity.
	 * @param entity entity
	 */
	void createEntity(ENTITY entity);

	/**
	 * Method for getting entity.
	 * @param id id
	 * @return entity.
	 */
	ENTITY getEntity(KEY id);

	/**
	 * Method for updating entity.
	 * @param entity entity
	 */
	void updateEntity(ENTITY entity);

	/**
	 * Method for removing entity.
	 * @param entity entity
	 */
	void removeEntity(ENTITY entity);

	/**
	 * Method for getting paged result.
	 * @param request request
	 * @return paged result with ready entities.
	 */
	PagedResult<ENTITY> getEntities(PagedRequest request);

}
