package org.parser.service.session;

import org.parser.domain.enumtype.api.API;
import org.parser.domain.enumtype.apimethodtype.APIMethodType;
import org.parser.domain.session.Session;
import org.parser.domain.user.User;

/**
 * Interface for declaring all operations with session user.
 * @author OlehZanevych
 */
public interface SessionService {

	
	/**
	 * Method for getting session details.
	 * @return session.
	 */
	Session getSession();
	
	/**
	 * Method for getting authorized user.
	 * @return session.
	 */
	User getUser();

	/**
	 * Method for verifying user access to API method.
	 * @param api
	 * @param apiMethodType
	 */
	void verifyAPIMethodAccess(API api, APIMethodType apiMethodType);
}
