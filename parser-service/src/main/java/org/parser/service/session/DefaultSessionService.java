package org.parser.service.session;

import java.text.MessageFormat;
import java.util.Map;

import org.parser.annotation.Service;
import org.parser.domain.enumtype.api.API;
import org.parser.domain.enumtype.apimethodtype.APIMethodType;
import org.parser.domain.session.Session;
import org.parser.domain.user.User;
import org.parser.domain.user.apicapability.UserAPICapability;
import org.parser.security.exception.AccessDeniedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Default implementation of session service.
 * @author OlehZanevych
 */
@Service("defaultSessionService")
public class DefaultSessionService implements SessionService {
	
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSessionService.class);
	
	@Override
	public User getUser() {
		User user = getSession().getUser();
		LOG.info(MessageFormat.format("Getting session user: {0}", user));
		return user;
	}
	
	@Override
	public Session getSession() {
		Session session = (Session) SecurityContextHolder.getContext().getAuthentication().getDetails();
		LOG.info(MessageFormat.format("Getting session: {0}", session));
		return session;
	}
	
	@Override
	public void verifyAPIMethodAccess(final API api, final APIMethodType apiMethodType) {
		Map<API, UserAPICapability> userAPICapabilities = getUser().getUserAPICapabilities();
		UserAPICapability userAPICapability = userAPICapabilities.get(api);
		if (userAPICapability == null || userAPICapability.getApiMethodType().compareTo(apiMethodType) < 0) {
			throw new AccessDeniedException("Access is denied to current user");
		}
	}
}
