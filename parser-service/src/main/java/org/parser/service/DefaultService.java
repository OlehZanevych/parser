package org.parser.service;

import org.parser.dao.dao.Dao;
import org.parser.pagination.PagedResult;
import org.parser.resource.search.PagedRequest;

/**
 * Default implementation of Service class.
 * NOTE: If you have some custom logic for any of methods below in this class,
 * you can easily extend this class, write some custom logic, and inject
 * it in services-context.xml.
 * 
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity, Please see parser-domain module.
 * @param <KEY> Key of corresponding 
 * @param <DAO> Dao
 */
public class DefaultService<ENTITY, KEY extends Number, DAO extends Dao<ENTITY, KEY>> implements Service<ENTITY, KEY> {

	private DAO dao;
	
	@Override
	public void createEntity(final ENTITY entity) {
		dao.save(entity);
	}

	@Override
	public ENTITY getEntity(final KEY id) {
		return dao.getEntityById(id);
	}

	@Override
	public void updateEntity(final ENTITY entity) {
		dao.update(entity);
	}

	@Override
	public void removeEntity(final ENTITY entity) {
		dao.delete(entity);
	}

	@Override
	public PagedResult<ENTITY> getEntities(final PagedRequest request) {
		return dao.getEntities(request);
	}

	public void setDao(final DAO defaultDao) {
		this.dao = defaultDao;
	}

	public DAO getDao() {
		return dao;
	}
	
}
