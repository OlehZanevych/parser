INSERT INTO t_system_users(id, login, password_hash, surname, name, middle_name, email, phone) VALUES 
	(1, 'admin', '$2a$10$ePg0csFsTK1l0lkYC.2d.u2Ru5Es7Gq8HBLcIVPYQkxCxauKNKn9u', 'Zanevych', 'Oleh', 'Bohdanovych', 'Oleh.Zanevych@gmail.com', '0671694179'), 
	(2, 'test', '$2a$10$IM24p5bo0Ensum.R7/uoN.UWWwT76hIGPKfeJq059ExG4YEFF4fbi', 'Test', 'Test', 'Testovych', 'Test@gmail.com', '0111111111')
