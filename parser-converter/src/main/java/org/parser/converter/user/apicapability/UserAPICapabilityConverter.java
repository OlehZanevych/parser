package org.parser.converter.user.apicapability;

import org.parser.annotation.Converter;
import org.parser.converter.AbstractConverter;
import org.parser.domain.enumtype.api.API;
import org.parser.domain.enumtype.apimethodtype.APIMethodType;
import org.parser.domain.user.apicapability.UserAPICapability;
import org.parser.resource.user.apicapability.UserAPICapabilityResource;

/**
 * Converter, that converts from UserAPICapability to UserAPICapabilityResource.
 * @author OlehZanevych
 */
@Converter("userAPICapabilityConverter")
public class UserAPICapabilityConverter extends AbstractConverter<UserAPICapability, UserAPICapabilityResource> {

	@Override
	public UserAPICapabilityResource convert(final UserAPICapability source, final UserAPICapabilityResource target) {
		
		API api = source.getApi();
		if (api != null) {
			target.setApi(api);
		}
		
		APIMethodType apiMethodType = source.getApiMethodType();
		if (apiMethodType != null) {
			target.setApiMethodType(apiMethodType);
		}
		
		return target;
	}

	@Override
	public UserAPICapabilityResource convert(final UserAPICapability source) {
		return convert(source, new UserAPICapabilityResource());
	}

}
