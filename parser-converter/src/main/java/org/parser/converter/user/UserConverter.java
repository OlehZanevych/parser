package org.parser.converter.user;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.parser.annotation.Converter;
import org.parser.converter.AbstractConverter;
import org.parser.converter.user.apicapability.UserAPICapabilityConverter;
import org.parser.domain.user.User;
import org.parser.domain.user.apicapability.UserAPICapability;
import org.parser.resource.user.UserResource;

/**
 * Converter, that converts from User to UserResource.
 * @author OlehZanevych
 */
@Converter("userConverter")
public class UserConverter extends AbstractConverter<User, UserResource> {

	@Resource(name = "userAPICapabilityConverter")
	private UserAPICapabilityConverter userAPICapabilityConverter;
	
	@Override
	public UserResource convert(final User source, final UserResource target) {
		
		String login = source.getLogin();
		if (login != null) {
			target.setLogin(login);
		}
		
		String surname = source.getSurname();
		if (surname != null) {
			target.setSurname(surname);
		}
		
		String name = source.getName();
		if (name != null) {
			target.setName(name);
		}
		
		String middleName = source.getMiddleName();
		if (middleName != null) {
			target.setMiddleName(middleName);
		}
		
		String email = source.getEmail();
		if (email != null) {
			target.setEmail(email);
		}
		
		String phone = source.getPhone();
		if (phone != null) {
			target.setPhone(phone);
		}
		
		Collection<UserAPICapability> userAPICapabilities = source.getUserAPICapabilities().values();
		if (userAPICapabilities != null) {
			target.setUserAPICapabilities(userAPICapabilities.stream()
					.map(i -> userAPICapabilityConverter.convert(i)).collect(Collectors.toList()));
		}
		
		return target;
	}

	@Override
	public UserResource convert(final User source) {
		return convert(source, new UserResource());
	}

}
