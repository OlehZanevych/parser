package org.parser.converter.post;

import org.parser.annotation.Converter;
import org.parser.converter.AbstractConverter;
import org.parser.domain.post.Post;
import org.parser.resource.post.PostResource;

/**
 * Converter, that converts from Post to PostResource.
 * @author OlehZanevych
 */
@Converter("postConverter")
public class PostConverter extends AbstractConverter<Post, PostResource> {

	@Override
	public PostResource convert(final Post source, final PostResource target) {
		
		Long id = source.getId();
		if (id != null) {
			target.setId(id);
		}
		
		String title = source.getTitle();
		if (title != null) {
			target.setTitle(title);
		}
		
		String mainPage = source.getMainPage();
		if (mainPage != null) {
			target.setMainPage(mainPage);
		}
		
		return target;
	}

	@Override
	public PostResource convert(final Post source) {
		return convert(source, new PostResource());
	}

}
