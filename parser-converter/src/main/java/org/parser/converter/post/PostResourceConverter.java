package org.parser.converter.post;

import org.parser.annotation.Converter;
import org.parser.converter.AbstractResourceConverter;
import org.parser.domain.post.Post;
import org.parser.resource.post.PostResource;

/**
 * Converter, that converts from PostResource to Post.
 * @author OlehZanevych
 */
@Converter("postResourceConverter")
public class PostResourceConverter extends AbstractResourceConverter<PostResource, Post, Long> {

	@Override
	public Post convert(final PostResource source, final Post target) {

		String title = source.getTitle();
		if (title != null) {
			if (title.isEmpty()) {
				target.setTitle(null);
			} else {
				target.setTitle(title);
			}
		}
		
		String mainPage = source.getMainPage();
		if (mainPage != null) {
			if (mainPage.isEmpty()) {
				target.setMainPage(null);
			} else {
				target.setMainPage(mainPage);
			}
		}
		
		return target;
	}

	@Override
	public Post convert(final PostResource source) {
		return convert(source, new Post());
	}

}
