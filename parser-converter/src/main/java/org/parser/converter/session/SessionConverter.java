package org.parser.converter.session;

import javax.annotation.Resource;

import org.parser.annotation.Converter;
import org.parser.converter.AbstractConverter;
import org.parser.converter.user.UserConverter;
import org.parser.domain.session.Session;
import org.parser.domain.user.User;
import org.parser.resource.session.SessionResource;

/**
 * Session converter.
 * @author OlehZanevych
 *
 */
@Converter("sessionConverter")
public class SessionConverter extends AbstractConverter<Session, SessionResource> {
	
	@Resource(name = "userConverter")
	private UserConverter userConverter;

	@Override
	public SessionResource convert(final Session source, final SessionResource target) {
		
		User user = source.getUser();
		if (user != null) {
			target.setUserResource(userConverter.convert(user));
		}
		
		return target;
	}

	@Override
	public SessionResource convert(final Session source) {
		return convert(source, new SessionResource());
	}

}
