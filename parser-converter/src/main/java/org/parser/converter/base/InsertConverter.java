package org.parser.converter.base;

import org.parser.annotation.Converter;
import org.parser.converter.AbstractConverter;
import org.parser.domain.ModelWithSimpleId;
import org.parser.resource.APIResource;

/**
 * Class, that is used to make some convertation
 * when insert method (POST) is called.
 * @author OlehZanevych
 *
 * @param <KEY> id type.
 * @param <S> resource type.
 * @param <T> class type.
 */
@Converter("insertConverter")
public class InsertConverter<KEY extends Number, S extends APIResource<KEY>, T extends ModelWithSimpleId<KEY>> extends AbstractConverter<S, T> {

	@Override
	public T convert(final S source, final T target) {
		return target;
	}

	@Override
	public T convert(final S source) {
		throw new UnsupportedOperationException("Method not allowed");
	}

}
