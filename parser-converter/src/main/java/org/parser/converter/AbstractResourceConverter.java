package org.parser.converter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.parser.domain.ModelWithSimpleId;
import org.parser.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract resource converter.
 * The reason I created this class is that I need to have
 * a convenient global mechanism to cascade update all child entities.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource
 * @param <ENTITY> Entity
 * @param <KEY> Identifier of Resource and Entity
 */
public abstract class AbstractResourceConverter<RESOURCE extends Resource<KEY>, ENTITY extends ModelWithSimpleId<KEY>,
	KEY extends Number> extends AbstractConverter<RESOURCE, ENTITY> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractResourceConverter.class);
	
	/**
     * Method for updating List of Entities.
     * @param resources List of Resources
     * @param entities List of existing Entities
     * @param setParentMethodName Name of method for setting parent Entity
     * @param parent Parent Entity
     */
	public void updateCollection(final List<RESOURCE> resources, final List<ENTITY> entities,
			final String setParentMethodName, final ModelWithSimpleId<KEY> parent) {
		
		List<RESOURCE> newResources;
		if (entities.isEmpty()) {
			newResources = resources;
		} else {
			newResources = new LinkedList<RESOURCE>();
			HashMap<KEY, RESOURCE> resourceWithId = new HashMap<KEY, RESOURCE>();
			for (RESOURCE resource: resources) {
				KEY id = resource.getId();
				if (id != null) {
					resourceWithId.put(id, resource);
				} else {
					newResources.add(resource);
				}
			}
			if (resourceWithId.values().isEmpty()) {
				entities.clear();
				newResources = resources;
			} else {
				Iterator<ENTITY> entitiesIterator = entities.iterator();
				while (entitiesIterator.hasNext()) {
					ENTITY entity = entitiesIterator.next();
					KEY id = entity.getId();
					RESOURCE resource = resourceWithId.get(id);
					if (resource != null) {
						convert(resource, entity);
						resourceWithId.remove(id);
					} else {
						entitiesIterator.remove();
					}
				}
				newResources.addAll(resourceWithId.values());
			}
		}
		List<ENTITY> newEntities = newResources.stream().map(i -> convert(i)).collect(Collectors.toList());
		if (!newEntities.isEmpty()) {
			Method setParentMethod;
			try {
				setParentMethod = newEntities.get(0).getClass().getDeclaredMethod(setParentMethodName, parent.getClass());
			} catch (NoSuchMethodException | SecurityException e) {
				LOG.info(e.getMessage(), e);
	            throw new IllegalArgumentException("Some problems with getting Method for setting parent reference during update collection. Please, contact dev team for fixing this issue");
			}
			for (ENTITY newEntity: newEntities) {
				try {
					setParentMethod.invoke(newEntity, parent);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					LOG.info(e.getMessage(), e);
		            throw new IllegalArgumentException("Some problems with setting parent reference during update collection. Please, contact dev team for fixing this issue");
				}
			}
			entities.addAll(newEntities);
		}
	}

}
