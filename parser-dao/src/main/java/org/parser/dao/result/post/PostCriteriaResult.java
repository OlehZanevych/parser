package org.parser.dao.result.post;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.ResultTransformer;
import org.parser.dao.annotation.CriteriaResult;
import org.parser.dao.result.DefaultCriteriaResult;
import org.parser.dao.result.transformer.DefaultResultTransformer;
import org.parser.domain.post.Post;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains methods for receiving
 * Posts from Hibernate Criteria.
 * @author OlehZanevych
 */
@CriteriaResult("postCriteriaResult")
public class PostCriteriaResult extends DefaultCriteriaResult<Post> {
	
	private static final DefaultResultTransformer RESULT_TRANSFORMER = new DefaultResultTransformer(Post.class);
	
	private static final ImmutableList<String> DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.addAll(COMMON_DEFAULT_FIELDS)
			.add("title")
			.add("mainPage")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
			.putAll(COMMON_PROJECTION_PROCESSORS)
		    .put("title", new ProjectionProcessor() {
		    	public void setProjection(final Criteria criteria, final ProjectionList projections, final HashSet<String> aliases) {
		    		projections.add(Projections.property("title"), "title");
		    	}
		    })
		    .put("mainPage", new ProjectionProcessor() {
		    	public void setProjection(final Criteria criteria, final ProjectionList projections, final HashSet<String> aliases) {
		    		projections.add(Projections.property("mainPage"), "mainPage");
		    	}
		    })
		    .build();
	
	private static final ImmutableMap<String, ParameterProcessor> PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
			.putAll(COMMON_PARAMETER_PROCESSORS)
			.put("title", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final HashSet<String> aliases) {
		    		criteria.add(createOneLikeRestrictions("title", value));
		    	}
		    })
			.put("mainPage", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final HashSet<String> aliases) {
		    		criteria.add(createOneLikeRestrictions("mainPage", value));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
			.putAll(COMMON_ORDER_PROCESSORS)
			.put("title", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final HashSet<String> aliases) {
		    		return "title";
		    	}
		    })
			.put("mainPage", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final HashSet<String> aliases) {
		    		return "mainPage";
		    	}
		    })
		    .build();
	
	@Override
	protected ResultTransformer getResultTransformer() {
		return RESULT_TRANSFORMER;
	}
	
	@Override
	protected List<String> getDefaultFields() {
		return DEFAULT_FIELDS;
	}
	
	@Override
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return PROJECTION_PROCESSORS;
	}
	
	@Override
	protected ImmutableMap<String, ParameterProcessor> getParameterProcessors() {
		return PARAMETER_PROCESSORS;
	}
	
	@Override
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return ORDER_PROCESSORS;
	}
	
}
