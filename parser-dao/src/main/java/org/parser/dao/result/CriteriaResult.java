package org.parser.dao.result;

import org.hibernate.Criteria;
import org.parser.domain.ModelWithSimpleId;
import org.parser.pagination.PagedResult;
import org.parser.resource.search.PagedRequest;

/**
 * Interface, that is used for receiving
 * data from Hibernate Criteria.
 * @author OlehZanevych
 * 
 * @param <ENTITY> Entity class.
 */
public interface CriteriaResult<ENTITY extends ModelWithSimpleId<?>> {
	
	/**
	 * Method for getting paged result for request.
	 * @param request PagedRequest
	 * @return PagedResult
	 */
	PagedResult<ENTITY> getResult(PagedRequest request, Criteria criteria);
	
}
