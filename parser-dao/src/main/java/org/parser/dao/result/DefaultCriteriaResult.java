package org.parser.dao.result;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;
import org.parser.domain.ModelWithSimpleId;
import org.parser.pagination.PagedResult;
import org.parser.resource.search.PagedRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Class, that contains common methods for receiving
 * data from Hibernate Criteria.
 * @author OlehZanevych
 * 
 * @param <ENTITY> Entity class.
 */
public class DefaultCriteriaResult<ENTITY extends ModelWithSimpleId<?>> implements CriteriaResult<ENTITY> {
	
	private static final String RESTRICTIONS_SEPARATOR = ";";
	private static final String CASES_SEPARATOR = ",";
	private static final String NULL = "NULL";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCriteriaResult.class);
	
	protected interface ProjectionProcessor {
		/**
		 * Method for setting projection.
		 * @param criteria Hibernate Criteria
		 * @param projections projections
		 * @param aliases set of aliases
		 */
		void setProjection(final Criteria criteria, final ProjectionList projections, final HashSet<String> aliases);
	}
	
	protected interface ParameterProcessor {
		/**
		 * Method for creation restriction for current value.
		 * @param value value
		 * @param criteria Hibernate Criteria
		 * @param aliases set of aliases
		 */
		void processParameter(final String value, final Criteria criteria, final HashSet<String> aliases);
	}
	
	protected interface OrderProcessor {
		/**
		 * Method for getting order field.
		 * @param criteria Hibernate Criteria
		 * @param aliases set of aliases
		 * @return order field
		 */
		String getOrderField(final Criteria criteria, final HashSet<String> aliases);
	}
	
	protected static final ImmutableList<String> COMMON_DEFAULT_FIELDS =
			ImmutableList.<String>builder()
			.add("id")
			.build();
	
	protected static final ImmutableMap<String, ProjectionProcessor> COMMON_PROJECTION_PROCESSORS =
			ImmutableMap.<String, ProjectionProcessor>builder()
		    .put("id", new ProjectionProcessor() {
		    	public void setProjection(final Criteria criteria, final ProjectionList projections, final HashSet<String> aliases) {
		    		projections.add(Projections.property("id"), "id");
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, ParameterProcessor> COMMON_PARAMETER_PROCESSORS =
			ImmutableMap.<String, ParameterProcessor>builder()
		    .put("id", new ParameterProcessor() {
		    	public void processParameter(final String value, final Criteria criteria, final HashSet<String> aliases) {
		    		criteria.add(createOneLongRestrictions("id", value));
		    	}
		    })
		    .build();
	
	protected static final ImmutableMap<String, OrderProcessor> COMMON_ORDER_PROCESSORS =
			ImmutableMap.<String, OrderProcessor>builder()
		    .put("id", new OrderProcessor() {
		    	public String getOrderField(final Criteria criteria, final HashSet<String> aliases) {
		    		return "id";
		    	}
		    })
		    .build();
	
	@Override
	@SuppressWarnings("unchecked")
	public PagedResult<ENTITY> getResult(final PagedRequest request, final Criteria criteria) {
		LOGGER.info("Getting paged result for request: {}", request);
		
		HashSet<String> aliases = new HashSet<String>();
		
		Map<String, String> parameters = request.getParameters();
		if (parameters != null && !parameters.isEmpty()) {
			Map<String, ParameterProcessor> parameterProcessors = getParameterProcessors();
			for (Entry<String, String> parameter : parameters.entrySet()) {
				String parameterName = parameter.getKey();
				String parameterValue = parameter.getValue();
				ParameterProcessor parameterProcessor = parameterProcessors.get(parameterName);
				if (parameterProcessor == null) {
					throw new IllegalArgumentException(MessageFormat.format("Parameter {0} is not allowed", parameterName));
				}
				parameterProcessor.processParameter(parameterValue, criteria, aliases);
			}
		}
		
		Long count = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		
		String[] fields = request.getFields();
		if (fields != null && fields.length == 1 && fields[0].equals("false")) {
			return new PagedResult<ENTITY>(count);
		} else {
			ProjectionList projections = Projections.projectionList();
			if (fields != null && fields.length == 1 && fields[0].equals("all")) {
				for (ProjectionProcessor projectionProcessor : getProjectionProcessors().values()) {
					projectionProcessor.setProjection(criteria, projections, aliases);
				}
			} else {
				String devReport = "";
				if (fields == null) {
					List<String> fieldsList = getDefaultFields();
					fields = fieldsList.toArray(new String[fieldsList.size()]);
					devReport = ". Please, contact dev team for fixing this issue";
				}
				Map<String, ProjectionProcessor> projectionProcessors = getProjectionProcessors();
				for (String field : fields) {
					ProjectionProcessor projectionProcessor = projectionProcessors.get(field);
					if (projectionProcessor == null) {
						throw new IllegalArgumentException(MessageFormat.format("Projection of {0} is not supported{1}", field, devReport));
					}
					projectionProcessor.setProjection(criteria, projections, aliases);
				}
			}
			criteria.setProjection(projections);
			criteria.setResultTransformer(getResultTransformer());
			
			Integer offset = request.getOffset();
			if (offset != null) {
				criteria.setFirstResult(offset);
			}
			
			Integer limit = request.getLimit();
			if (limit != null) {
				criteria.setMaxResults(limit);
			}
			
			String[] orders = request.getOrders();
			if (orders != null) {
				Map<String, OrderProcessor> orderProcessors = getOrderProcessors();
				for (String order : orders) {
					String[] orderParts = order.split("-");
					OrderProcessor orderProcessor = orderProcessors.get(orderParts[0]);
					if (orderProcessor == null) {
						throw new IllegalArgumentException(MessageFormat.format("Ordering by {0} is not allowed", orderParts[0]));
					}
					if (orderParts.length == 2 && orderParts[1].equals("desc")) {
						criteria.addOrder(Order.desc(orderProcessor.getOrderField(criteria, aliases)));
					} else {
						criteria.addOrder(Order.asc(orderProcessor.getOrderField(criteria, aliases)));
					}
				}
			}
			
			List<ENTITY> results = criteria.list();
			
			return new PagedResult<ENTITY>(offset, limit, count, results);
		}
	}
	
	protected ResultTransformer getResultTransformer() {
		return null;
	}
	
	protected List<String> getDefaultFields() {
		return COMMON_DEFAULT_FIELDS;
	}
	
	protected Map<String, ProjectionProcessor> getProjectionProcessors() {
		return COMMON_PROJECTION_PROCESSORS;
	}
	
	protected Map<String, ParameterProcessor> getParameterProcessors() {
		return COMMON_PARAMETER_PROCESSORS;
	}
	
	protected Map<String, OrderProcessor> getOrderProcessors() {
		return COMMON_ORDER_PROCESSORS;
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with all like restrictions.
	 * @param field field
	 * @param requirements requirements
	 * @return restriction
	 */
	protected static Criterion createOneLikeRestrictions(final String field, final String requirements) {
		Criterion[] restrictions = Arrays.stream(requirements.split(RESTRICTIONS_SEPARATOR))
				.map(i -> createOneLikeRestriction(field, i)).toArray(Criterion[]::new);
		return Restrictions.and(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with all long restrictions.
	 * @param field field
	 * @param requirements requirements
	 * @return restriction
	 */
	protected static Criterion createOneLongRestrictions(final String field, final String requirements) {
		Criterion[] restrictions = Arrays.stream(requirements.split(RESTRICTIONS_SEPARATOR))
				.map(i -> createOneLongRestriction(field, i)).toArray(Criterion[]::new);
		return Restrictions.and(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with all double restrictions.
	 * @param field field
	 * @param requirements requirements
	 * @return restriction
	 */
	protected static Criterion createOneDoubleRestrictions(final String field, final String requirements) {
		Criterion[] restrictions = Arrays.stream(requirements.split(RESTRICTIONS_SEPARATOR))
				.map(i -> createOneDoubleRestriction(field, i)).toArray(Criterion[]::new);
		return Restrictions.and(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with all enum restrictions.
	 * @param field field
	 * @param enumType enum type
	 * @param requirements requirements
	 * @return restriction
	 */
	protected static Criterion createOneEnumRestrictions(final String field, final Class<? extends Enum> enumType,
			final String requirements) {
		
		Criterion[] restrictions = Arrays.stream(requirements.split(RESTRICTIONS_SEPARATOR))
				.map(i -> createOneEnumRestriction(field, enumType, i)).toArray(Criterion[]::new);
		return Restrictions.and(restrictions);
	}
	
	/**
	 * Method for creating restriction for like match current field
	 * with one of variants.
	 * @param field field
	 * @param variants variants
	 * @return restriction
	 */
	protected static Criterion createOneLikeRestriction(final String field, final String variants) {
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR))
				.map(i -> createLikeRestriction(field, i)).toArray(Criterion[]::new);
		return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with one of parsed long range variants.
	 * @param field field
	 * @param variants variants
	 * @return restriction
	 */
	protected static Criterion createOneLongRestriction(final String field, final String variants) {
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					int length = i.length();
					if (length > 2) {
						switch (i.substring(0, 2)) {
				            case "<=":
				            	return createLessOrEqualRestriction(field, Long.parseLong(i.substring(2)));
				            case ">=":
				            	return createGreaterOrEqualRestriction(field, Long.parseLong(i.substring(2)));
						}
					}
					if (length > 1) {
						switch (i.substring(0, 1)) {
				            case "<":
				            	return createLessRestriction(field, Long.parseLong(i.substring(1)));
				            case ">":
				            	return createGreaterRestriction(field, Long.parseLong(i.substring(1)));
						}
					}
					return i.equals(NULL) ? createIsNullRestriction(field)
							: createEqualRestriction(field, Long.parseLong(i));
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with one of parsed double range variants.
	 * @param field field
	 * @param variants variants
	 * @return restriction
	 */
	protected static Criterion createOneDoubleRestriction(final String field, final String variants) {
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					int length = i.length();
					if (length > 2) {
						switch (i.substring(0, 2)) {
				            case "<=":
				            	return createLessOrEqualRestriction(field, Double.parseDouble(i.substring(2)));
				            case ">=":
				            	return createGreaterOrEqualRestriction(field, Double.parseDouble(i.substring(2)));
						}
					}
					if (length > 1) {
						switch (i.substring(0, 1)) {
				            case "<":
				            	return createLessRestriction(field, Double.parseDouble(i.substring(1)));
				            case ">":
				            	return createGreaterRestriction(field, Double.parseDouble(i.substring(1)));
						}
					}
					return i.equals(NULL) ? createIsNullRestriction(field)
							: createEqualRestriction(field, Double.parseDouble(i));
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating restriction for match current field
	 * with one of parsed enum variants.
	 * @param field field
	 * @param enumType enum type
	 * @param variants variants
	 * @return restriction
	 */
	protected static Criterion createOneEnumRestriction(final String field, final Class<? extends Enum> enumType, final String variants) {
		Criterion[] restrictions = Arrays.stream(variants.split(CASES_SEPARATOR)).map(i -> {
					if (i.equals(NULL)) {
						return createIsNullRestriction(field);
					}
					try {
						return createEqualRestriction(field, Enum.valueOf(enumType, i));
					} catch (Exception e) {
						throw new IllegalArgumentException(MessageFormat.format("Value {0} is not allowed for filtering for enum field {1}", i, field));
					}
				})
				.toArray(Criterion[]::new);
				return Restrictions.or(restrictions);
	}
	
	/**
	 * Method for creating like restriction for current field and value.
	 * @param field field
	 * @param value value
	 * @return restriction
	 */
	protected static Criterion createLikeRestriction(final String field, final String value) {
		return value.equals(NULL) ? createIsNullRestriction(field)
				: Restrictions.ilike(field, value, MatchMode.ANYWHERE);
	}
	
	/**
	 * Method for creating equal restriction for current field and value.
	 * @param field field
	 * @param value value
	 * @return restriction
	 */
	protected static Criterion createEqualRestriction(final String field, final Object value) {
		return Restrictions.eq(field, value);
	}
	
	/**
	 * Method for creating less or equal restriction for current field and value.
	 * @param field field
	 * @param value value
	 * @return restriction
	 */
	protected static Criterion createLessOrEqualRestriction(final String field, final Object value) {
		return Restrictions.le(field, value);
	}
	
	/**
	 * Method for creating less restriction for current field and value.
	 * @param field field
	 * @param value value
	 * @return restriction
	 */
	protected static Criterion createLessRestriction(final String field, final Object value) {
		return Restrictions.lt(field, value);
	}
	
	/**
	 * Method for creating greater or equal restriction for current field and value.
	 * @param field field
	 * @param value value
	 * @return restriction
	 */
	protected static Criterion createGreaterOrEqualRestriction(final String field, final Object value) {
		return Restrictions.ge(field, value);
	}
	
	/**
	 * Method for creating greater restriction for current field and value.
	 * @param field field
	 * @param value value
	 * @return restriction
	 */
	protected static Criterion createGreaterRestriction(final String field, final Object value) {
		return Restrictions.gt(field, value);
	}
	
	/**
	 * Method for creating restriction for checking whether current field is null.
	 * @param field field
	 * @return restriction
	 */
	protected static Criterion createIsNullRestriction(final String field) {
		return Restrictions.isNull(field);
	}
	
	/**
	 * Method for creating alias for current field.
	 * @param field field
	 * @param alias alias
	 * @param criteria Hibernate Criteria
	 * @param aliases set of aliases
	 */
	protected static void createAlias(final String field, final String alias, final Criteria criteria,
			final HashSet<String> aliases) {
		
		if (!aliases.contains(alias)) {
			criteria.createAlias(field, alias);
			aliases.add(alias);
		}
	}
}
