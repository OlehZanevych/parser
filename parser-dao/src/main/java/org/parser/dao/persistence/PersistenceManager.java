package org.parser.dao.persistence;

import org.hibernate.Criteria;
import org.parser.domain.ModelWithSimpleId;
/**
 * Interface, that is used to encapsulate all work
 * with entity manager.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity Class
 * @param <KEY> Identifier Class
 */
public interface PersistenceManager<ENTITY extends ModelWithSimpleId<?>, KEY extends Number> {

	/**
	 * Method for creating entity.
	 * @param entity Instance of entity
	 * @return Entity with Id
	 */
    ENTITY create(ENTITY entity);

    /**
     * Finds entity from database by identifier.
     * @param clazz Class instance
     * @param id identifier
     * @return Entity instance
     */
    ENTITY findById(Class<ENTITY> clazz, KEY id);

    /**
     * Updates entity.
     * @param entity entity instance
     * @return Updated instance
     */
    ENTITY update(ENTITY entity);

    /**
     * Removes entity.
     * @param entity entity instance
     */
    void remove(ENTITY entity);
	
    /**
     * Create Hibernate Criteria.
     * @param clazz Class instance
     * @return Hibernate Criteria instance
     */
	Criteria createCriteria(Class<ENTITY> clazz);
}
