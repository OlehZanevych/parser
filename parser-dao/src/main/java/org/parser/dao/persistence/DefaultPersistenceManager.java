package org.parser.dao.persistence;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.parser.dao.exception.EntityNotFoundException;
import org.parser.domain.ModelWithSimpleId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Persistence Manager to work with entity manager.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <KEY> Identifier class.
 */
@Repository("persistenceManager")
public class DefaultPersistenceManager<ENTITY extends ModelWithSimpleId<KEY>, KEY extends Number> implements PersistenceManager<ENTITY, KEY> {
    
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPersistenceManager.class);

	@PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public ENTITY create(final ENTITY entity) {
		LOGGER.info("Saving entity for class: {}", entity.getClass());
		entityManager.persist(entity);
		return entity;
    }

    @Override
    public ENTITY findById(final Class<ENTITY> clazz, final KEY id) {
    	LOGGER.info("Get entity: {} by id: {}", clazz, id);
        ENTITY entity = entityManager.find(clazz, id);
        if (entity == null) {
        	LOGGER.error("Entity doesn't exist, {}, {}", clazz.getSimpleName());
        	throw new EntityNotFoundException("Entity doesn't exist", clazz, id);
        }
        return entity;
    }

    @Override
    public ENTITY update(final ENTITY entity) {
    	LOGGER.info("Updating entity for class: {}", entity.getClass());
        return entityManager.merge(entity);
    }

    @Override
    public void remove(final ENTITY entity) {
    	LOGGER.info("Removing entity for class: {}", entity.getClass());
    	entityManager.remove(entity);
    }

    @Override
    public Criteria createCriteria(final Class<ENTITY> clazz) {
    	LOGGER.info("Create Hibernate Criteria for class: {}", clazz);
    	return entityManager.unwrap(Session.class).createCriteria(clazz);
    }
	
}
