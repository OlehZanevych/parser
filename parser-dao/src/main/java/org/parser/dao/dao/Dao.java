package org.parser.dao.dao;

import org.parser.pagination.PagedResult;
import org.parser.resource.search.PagedRequest;

/**
 * Interface, that has all methods, that are needed 
 * to work with entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity class.
 * @param <KEY> Identifier class, that extends Number.
 */
public interface Dao<ENTITY, KEY extends Number> {

	/**
	 * Method for finding Entities by Id.
	 * @param id identifier.
	 * @return Entity.
	 */
	ENTITY getEntityById(KEY id);
	
	/**
	 * Method for saving entity.
	 * @param entity entity.
	 */
	void save(ENTITY entity);
	
	/**
	 * Method for updating entity.
	 * @param entity entity.
	 */
	void update(ENTITY entity);
	
	/**
	 * Method for deleting entity.
	 * @param entity entity.
	 */
	void delete(ENTITY entity);
	
	/**
	 * Method for getting paged Result.
	 * @param request paged request
	 * @return paged result.
	 */
	PagedResult<ENTITY> getEntities(PagedRequest request);
}

