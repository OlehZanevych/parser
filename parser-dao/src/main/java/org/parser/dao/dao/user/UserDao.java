package org.parser.dao.dao.user;

import org.parser.dao.dao.Dao;
import org.parser.domain.user.User;

/**
 * User dao interface.
 * @author OlehZanevych
 */
public interface UserDao extends Dao<User, Long> {

	/**
	 * Method for getting user by login.
	 * @param login login
	 * @return User.
	 */
	User getUserByLogin(String login);
	
}
