package org.parser.dao.dao.user;

import org.hibernate.criterion.Restrictions;
import org.parser.dao.dao.DefaultDao;
import org.parser.domain.user.User;

/**
 * Default User Dao.
 * @author OlehZanevych
 */
public class DefaultUserDao extends DefaultDao<User, Long> implements UserDao {
	
	@Override
	public User getUserByLogin(final String login) {
		return (User) getPersistenceManager().createCriteria(User.class)
				.add(Restrictions.eq("login", login)).uniqueResult();
	}

}
