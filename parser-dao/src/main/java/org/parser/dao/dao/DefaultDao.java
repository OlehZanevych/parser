package org.parser.dao.dao;

import org.parser.dao.persistence.PersistenceManager;
import org.parser.dao.result.CriteriaResult;
import org.parser.domain.ModelWithSimpleId;
import org.parser.pagination.PagedResult;
import org.parser.resource.search.PagedRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Abstract implementation, that has
 * all methods for convenient work with entities.
 * @author OlehZanevych
 *
 * @param <ENTITY> Entity type.
 * @param <KEY> Identifier class.
 */
public class DefaultDao<ENTITY extends ModelWithSimpleId<KEY>, KEY extends Number> implements Dao<ENTITY, KEY> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDao.class);
	
	private PersistenceManager<ENTITY, KEY> persistenceManager;
	
	private Class<ENTITY> entityClass;
	
	private CriteriaResult<ENTITY> criteriaResult;
	
	@Override
	public ENTITY getEntityById(final KEY id) {
		LOGGER.info("Getting {}.entity wit id", getEntityClass().getSimpleName(), id);
		return persistenceManager.findById(getEntityClass(), id);
	}

	@Override
	public void save(final ENTITY entity) {
		LOGGER.info("Saving {}.entity: {}", getEntityClass().getSimpleName(), entity);
		persistenceManager.create(entity);
	}

	@Override
	public void update(final ENTITY entity) {
		LOGGER.info("Updating {}.entity with id: {}, {}", getEntityClass().getSimpleName(), entity.getId(), entity);
		persistenceManager.update(entity);
	}

	@Override
	public void delete(final ENTITY entity) {
		LOGGER.info("Removing {}.entity with id: {}, {}", getEntityClass().getSimpleName(), entity.getId(), entity);
		persistenceManager.remove(entity);
	}

	@Override
	public PagedResult<ENTITY> getEntities(final PagedRequest request) {
		LOGGER.info("Getting paged result for {}", getEntityClass().getSimpleName());
		return criteriaResult.getResult(request, persistenceManager.createCriteria(entityClass));
	}
	
	public PersistenceManager<ENTITY, KEY> getPersistenceManager() {
		return persistenceManager;
	}

	public void setPersistenceManager(final PersistenceManager<ENTITY, KEY> persistenceManager) {
		this.persistenceManager = persistenceManager;
	}

	public Class<ENTITY> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(final Class<ENTITY> entityClass) {
		this.entityClass = entityClass;
	}

	public CriteriaResult<ENTITY> getCriteriaResult() {
		return criteriaResult;
	}

	public void setCriteriaResult(final CriteriaResult<ENTITY> criteriaResult) {
		this.criteriaResult = criteriaResult;
	}
	
}
